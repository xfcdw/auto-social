<?php
require('lib/config.php');
?>
<!DOCTYPE html>
<html>
<?php include('lib/header.phtml'); ?>
<body id="page-top" class="landing-page no-skin-config">
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top navbar-expand-md" role="navigation">
            <div class="container">
                <a class="navbar-brand" href="<?=$config['host'];?>"><?=$config['name'];?></a>
                <div class="navbar-header page-scroll">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse justify-content-end" id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="nav-link page-scroll" href="#page-top">Home</a></li>
                        <li><a class="nav-link page-scroll" href="#features">Fitur</a></li>
                        <li><a class="nav-link page-scroll" href="#testimonials">Testimoni</a></li>
                        <li><a class="nav-link page-scroll" href="#pricing">Harga</a></li>
                        <li><a class="nav-link page-scroll" href="#contact">Kontak</a></li>
                    </ul>
                </div>
            </div>
        </nav>
</div>
<div id="inSlider" class="carousel slide" data-ride="carousel">
    
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Get followers<br>
                         on Instagram<br>
                         with our Instagram bot</h1>
                    <p>Automatically Like, Follow, Comment, Unfollow, DM, and Post<br>in an Instant with <?=$config['name'];?></p>
                    
                </div>
                <div class="carousel-image wow zoomIn animated" style="visibility: visible;">
                    <img src="https://web-nthanfp.c9users.io/auto-social/assets/img/landing/laptop.png" alt="laptop">
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>

        </div>
        
    </div>
    
    
</div>

<section id="features" class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1><span class="navy">Fitur-fitur <?=$config['name'];?></span></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center wow fadeInLeft">
            <div>
                <i class="fa fa-user-plus features-icon"></i>
                <h2>Auto Follow</h2>
                <p>Fitur ini memungkinkan akun Instagram Anda memfollow otomatis user yang Anda targetkan.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-heart features-icon"></i>
                <h2>Auto Like</h2>
                <p>Fitur ini memungkinkan akun Instagram Anda menyukai postingan yang ada di beranda secara otomatis.</p>
            </div>
        </div>
        <div class="col-md-6 text-center  wow zoomIn">
            <img src="<?=$config['host'];?>/assets/img/landing/perspective.png" alt="dashboard" class="img-fluid">
        </div>
        <div class="col-md-3 text-center wow fadeInRight">
            <div>
                <i class="fa fa-user-times features-icon"></i>
                <h2>Auto Unfollow</h2>
                <p>Fitur ini memungkinkan akun Instagram Anda unfollow user-user yang ada di daftar following Anda.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-refresh features-icon"></i>
                <h2>Schedule Repost</h2>
                <p>Fitur ini memungkinkan akun Instagram Anda untuk menjadwalkan postingan-postingan yang ingin Anda repost. Anda hanya perlu setting sekali dan tinggalkan, maka semua postingan Anda akan terposting otomatis sesuai dengan waktu yang Anda tetapkan.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Temukan fitur hebat lainnya</h1>
            <p>Ayo daftar sekarang, tunggu apa lagi...</p>
        </div>
    </div>
</section>

<section id="testimonials" class="comments gray-section" style="margin-top: 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Apa Kata Mereka</h1>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-4">
                <div class="bubble">
                    "Ini kerennn. Auto Social membantu saya fokus dalam bisnis saya. Saya tidak lagi di ponsel saya sepanjang hari dengan Instagram."
                </div>
                <div class="comments-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="<?=$config['host'];?>/assets/img/landing/avatar3.jpg">
                    </a>
                    <div class="media-body">
                        <div class="commens-name">
                            Vannesa
                        </div>
                        <small class="text-muted">Pengguna <?=$config['name'];?></small>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="bubble">
                    "Ini benar-benar menghemat waktu dan tenaga saya. Saya sangat puas dengan tools ini. Layanan ini sangat baik!."
                </div>
                <div class="comments-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="<?=$config['host'];?>/assets/img/landing/avatar1.jpg">
                    </a>
                    <div class="media-body">
                        <div class="commens-name">
                            Andrew Williams
                        </div>
                        <small class="text-muted">Pengguna <?=$config['name'];?></small>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="bubble">
                    "Satu hal yang saya suka adalah "Scheduled Posts" dengan fitur ini saya dapat memosting postingan saya sehingga ketika saya bepergian atau bekerja dan sebagainya AutoSocial dapat memosting di Instagram saya."
                </div>
                <div class="comments-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="<?=$config['host'];?>/assets/img/landing/avatar2.jpg">
                    </a>
                    <div class="media-body">
                        <div class="commens-name">
                            Nathan
                        </div>
                        <small class="text-muted">Pengguna <?=$config['name'];?></small>
                    </div>
                </div>
            </div>



        </div>
    </div>

</section>

<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Kenapa Harus <?=$config['name'];?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small><?=$config['name'];?></small>
                <h2>All In One </h2>
                <i class="fa fa-list big-icon float-right"></i>
                <p>Semua fitur yang Anda butuhkan tersedia dalam satu tempat. Anda tidak perlu menjalankan banyak aplikasi atau browser. Praktis..</p>
            </div>
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small><?=$config['name'];?></small>
                <h2>Tampilan Responsive </h2>
                <i class="fa fa-rocket big-icon float-right"></i>
                <p>Layanan <?=$config['name'];?> ini dapat anda akses melalui PC, Tablet, Smartphone dan Perangkat Anda lainnya.</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-5 features-text">
                <small><?=$config['name'];?></small>
                <h2>Aman </h2>
                <i class="fa fa-shield big-icon float-right"></i>
                <p>Keamanan Pengguna <?=$config['name'];?> adalah prioritas kami, Kami Tidak menyimpan password atau data penting anda.</p>
            </div>
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small><?=$config['name'];?></small>
                <h2>Mudah Digunakan </h2>
                <i class="fa fa-clock-o big-icon float-right"></i>
                <p>Sangat mudah digunakan. Anda hanya perlu setting sekali, dan kemudian tinggalkan</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-5 features-text">
                <small><?=$config['name'];?></small>
                <h2>Harga Terjangkau </h2>
                <i class="fa fa-money big-icon float-right"></i>
                <p>Harga sangat terjangkau dengan fitur terlengkap yang bisa Anda temukan di pasaran.</p>
            </div>
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small><?=$config['name'];?></small>
                <h2>Full Support </h2>
                <i class="fa fa-envelope-o big-icon float-right"></i>
                <p>Tim Customer Service profesional yang selalu siap membantu Anda jika Anda mengalami kendala.</p>
            </div>
        </div>
    </div>

</section>
<section id="pricing" class="pricing">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>App Pricing</h1>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 wow zoomIn">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">
                        Basic
                    </li>
                    <li class="pricing-price">
                        <h2><span>35K</span> / month</h2>
                    </li>
                    <li>3 Akun Instagram</li>
                    <li>Auto Follow</li>
                    <li>Auto Like</li>
                    <li>Auto Unfollow</li>
                    <li>Auto Repost</li>
                    <li>Schedule Repost</li>
                    <li>Schedule Post</li>
                    <li>Unlimited Storage</li>
                    <li>
                        <a class="btn btn-primary btn-xs" href="#">Beli Sekarang</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-6 wow zoomIn">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">
                        Pro
                    </li>
                    <li class="pricing-price">
                        <h2><span>40K</span> / month</h2>
                    </li>
                    <li>5 Akun Instagram</li>
                    <li>Auto Follow</li>
                    <li>Auto Like</li>
                    <li>Auto Unfollow</li>
                    <li>Auto Repost</li>
                    <li>Schedule Repost</li>
                    <li>Schedule Post</li>
                    <li>Unlimited Storage</li>
                    <li>
                        <a class="btn btn-primary btn-xs" href="#">Beli Sekarang</a>
                    </li>
                </ul>
            </div>

        </div>
        
    </div>

</section>

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
            </div>
        </div>
        <div class="row m-b-lg justify-content-center">
            <div class="col-lg-3 ">
                <address>
                    <strong><span class="navy"><?=$config['name'];?></span></strong><br/>
                    Bandung<br/>
                    Jawa Barat, ID<br/>
                    <abbr title="Phone">Phone:</abbr> +62 856-4687-7046
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">
                    Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:test@nthanfp@gmail.com" class="btn btn-primary">Send us mail</a>
                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center m-t-lg m-b-lg">
                <p><strong>&copy; <?=date('Y');?> <?=$config['name'];?></strong><br/> Made with <i class="fa fa-heart" style="color:red;"></i> by <b>axec0de</b> </p>
            </div>
        </div>
    </div>
</section>

    <!-- Mainly scripts -->
    <script src="<?=$config['host'];?>/assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?=$config['host'];?>/assets/js/popper.min.js"></script>
    <script src="<?=$config['host'];?>/assets/js/bootstrap.js"></script>
    <script src="<?=$config['host'];?>/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=$config['host'];?>/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <!-- Custom and plugin javascript -->
    <script src="<?=$config['host'];?>/assets/js/inspinia.js"></script>
    <script src="<?=$config['host'];?>/assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?=$config['host'];?>/assets/js/plugins/wow/wow.min.js"></script>
    <script>
    
        $(document).ready(function () {
    
            $('body').scrollspy({
                target: '#navbar',
                offset: 80
            });
    
            // Page scrolling feature
            $('a.page-scroll').bind('click', function(event) {
                var link = $(this);
                $('html, body').stop().animate({
                    scrollTop: $(link.attr('href')).offset().top - 50
                }, 500);
                event.preventDefault();
                $("#navbar").collapse('hide');
            });
        });
    
        var cbpAnimatedHeader = (function() {
            var docElem = document.documentElement,
                    header = document.querySelector( '.navbar-default' ),
                    didScroll = false,
                    changeHeaderOn = 200;
            function init() {
                window.addEventListener( 'scroll', function( event ) {
                    if( !didScroll ) {
                        didScroll = true;
                        setTimeout( scrollPage, 250 );
                    }
                }, false );
            }
            function scrollPage() {
                var sy = scrollY();
                if ( sy >= changeHeaderOn ) {
                    $(header).addClass('navbar-scroll')
                }
                else {
                    $(header).removeClass('navbar-scroll')
                }
                didScroll = false;
            }
            function scrollY() {
                return window.pageYOffset || docElem.scrollTop;
            }
            init();
    
        })();
    
        // Activate WOW.js plugin for animation on scrol
        new WOW().init();
    
    </script>
    <?php include('lib/footer.phtml'); ?>
    </body>
</html>