<?php
function rupiah($angka)
{
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

function random($length) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

function random_number($length) {
	$str = "";
	$characters = array_merge(range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

function random_code($length) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

function random_month() {
	$month = mt_rand(1,12);
	if($month == '1'){
		$month = '01';
	} else if($month == '2'){
		$month = '02';
	} else if($month == '3'){
		$month = '03';
	} else if($month == '4'){
		$month = '04';
	} else if($month == '5'){
		$month = '05';
	} else if($month == '6'){
		$month = '06';
	} else if($month == '7'){
		$month = '07';
	} else if($month == '8'){
		$month = '08';
	} else if($month == '9'){
		$month = '09';
	} 

	return $month;
}

function random_year() {
	$year = mt_rand(1,5);
	$now  = date('y');
	if($year == '1'){
		$year = $now+1;
	} else if($year == '2'){
		$year = $now+2;
	} else if($year == '3'){
		$year = $now+3;
	} else if($year == '4'){
		$year = $now+4;
	} else if($year == '5'){
		$year = $now+5;
	} else {
		$year = '';
	}

	return $year;
}

function zip($folder, $name)
{
    $name     = $name;
    $rootPath = realpath($folder);
    $zip      = new ZipArchive();
    $zip->open('' . $name . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);
    foreach ($files as $name => $file) {
        if (!$file->isDir()) {
            $filePath     = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            $zip->addFile($filePath, $relativePath);
        }
    }
    $zip->close();
}

function deleteDir($dirPath)
{
    if (!is_dir($dirPath)) {
        if (file_exists($dirPath) !== false) {
            unlink($dirPath);
        }
        return;
    }
    
    if ($dirPath[strlen($dirPath) - 1] != '/') {
        $dirPath .= '/';
    }
    
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    
    rmdir($dirPath);
}

function axemail($apikey, $receiver_email, $sender_name, $sender_email, $subject, $body, $html){
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://nthanfp.me/api/post/sendMail/",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "apikey=".$apikey."&receiver_email=".$receiver_email."&sender_name=".$sender_name."&sender_email=".$sender_email."&subject=".$subject."&body=".$body."&is_html=".$html."",
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded"
		),
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
		return "cURL Error #:" . $err;
	} else {
		return $response;
	}
}