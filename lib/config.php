<?php
date_default_timezone_set('Asia/Jakarta');
error_reporting(0);
$cfg_mt = 0; // Maintenance? 1 = ya 0 = tidak
if($cfg_mt == 1) {
    die('SITE IS UNDER MAINTENANCE. Please check back in sometime.');
}
// config website
if(php_sapi_name() === 'cli'){
    $config['host']   = 'https://veonspanel.com/tools';
} elseif($_SERVER['HTTP_HOST'] == 'veonspanel.com'){
    $config['host']   = 'https://veonspanel.com/tools';
} elseif($_SERVER['HTTP_HOST'] == 'c9nathan-iozalnvz923836.codeanyapp.com'){
    $config['host']   = 'https://c9nathan-iozalnvz923836.codeanyapp.com/c9sdk/auto-social';
} else {
    $config['host']   = 'https://veonspanel.com/tools';
}
$config['title']      = 'AutoSocial | Instagram Marketing';
$config['name']       = 'AutoSocial';
$config['namev2']     = 'AutoSocial';
$config['desc']       = 'AutoSocial Smart Tools for Instagram Account';
$config['author']     = 'axec0de';
$config['logo_text']  = 'Asc';
$config['about']      = ''.$config['name'].' adalah sebuah platform pembayaran untuk berbagai layanan social media marketing & distributor layanan pulsa yang bergerak terutama di Indonesia.<br />Dengan bergabung bersama kami, Anda dapat menjadi penyedia jasa social media atau reseller social media seperti jasa penambah Followers, Likes, dll & menjadi agen distributor pulsa seperti pulsa, paket internet, voucher game, token pln, dll.<br />Saat ini tersedia berbagai layanan untuk social media terpopuler seperti Instagram, Facebook, Twitter, Youtube, dll. & layanan pulsa, paket internet, voucher game,token pln, dll.';
//recaptcha
$recaptcha['secret']  = '6LciH4kUAAAAAFyV49DaADskkRZsh2IYJihAO5_L';
$recaptcha['sitekey'] = '6LciH4kUAAAAAMCs5z8BXxzoAWAESJAwR94mHHCB';
//date
$today                = date('Y-m-d H:i:s');
// database
if($_SERVER['HTTP_HOST'] == 'veonspanel.com'){
    $db_server        = 'localhost';
} elseif($_SERVER['HTTP_HOST'] == 'c9nathan-iozalnvz923836.codeanyapp.com'){
    $db_server        = 'veonspanel.com';
} else {
    $db_server        = 'localhost';
}
$db_user              = 'tools';
$db_password          = 'hahalol123!';
$db_name              = 'IGtools';
// date & time
$date                 = date('Y-m-d');
$time                 = date('H:i:s');
// require
require('database.php');
require('function.php');
require('igfunc.php');