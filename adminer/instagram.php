<?php
require('../lib/config.php');
session_start();
include('../lib/akses.php');
if($data_user['level'] <> 'admin'){
    header("Location: ".$config['host']);
    exit;
}
if($_GET['delete']){
    if(!is_numeric($_GET['delete'])){
        header("Location: ".$config['host']."/adminer/instagram.php");
        exit;
    }
    $id  = trim_replace($_GET['delete']);
    $uid = trim_replace($_GET['uid']);
    $t   = mysqli_query($conn, "SELECT uplink FROM tbl_instagram WHERE row='$id'");
    $ew  = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/adminer/instagram.php");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/adminer/instagram.php");
        exit;
    }
    mysqli_query($conn, "DELETE FROM tbl_instagram WHERE row='$id'");
    mysqli_query($conn, "DELETE FROM tbl_liketask WHERE row='$uid'");
    mysqli_query($conn, "DELETE FROM tbl_followtask WHERE row='$uid'");
    mysqli_query($conn, "DELETE FROM tbl_posttask WHERE row='$uid'");
    header("Location: ".$config['host']."/adminer/instagram.php");
}
?>
<!DOCTYPE html>
<html>
<?php include('../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../lib/sidebar.phtml'); ?>
        <?php include('../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Admin / Manage Instagram Account</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Manage Instagram Account</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <table id="example1" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Uplink</th>
                                        <th>Identity</th>
                                        <th>User Agent</th>
                                        <th>Device ID</th>
                                        <th>is Verified</th>
                                        <th>Total Like Task</th>
                                        <th>Total Follow Task</th>
                                        <th>Total Post Task</th>
                                        <th>Insert Date</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalmembers = mysqli_query($conn, "SELECT * FROM tbl_instagram");
                                    $t            = 0;
                                    $ew           = mysqli_num_rows($totalmembers);
                                    if($ew < 1){
                                        print '<tr><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td></tr>';
                                    } else {
                                      while($fetch = mysqli_fetch_array($totalmembers)){
                                        $t++;
                                        if($fetch['verifikasi']<1){
                                          $a           = '<font color="orange">False</font>';
                                          $req         = proccess(1, $fetch['useragent'], 'feed/timeline/', $fetch['cookies']);
                                          $is_verified = (json_decode($req[1])->status<>ok) ? 0 : 1;
                                          mysqli_query($conn, "UPDATE tbl_instagram SET verifikasi=$is_verified WHERE row=$fetch[row]");
                                        } else {
                                          $a = '<font color="green">True</font>';
                                        }
                                        $uplink        = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_members WHERE id='".$fetch['uplink']."'"));
                                        $liketask      = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tbl_liketask WHERE user='".$fetch['id']."'"));
                                        $followtask    = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tbl_followtask WHERE user='".$fetch['id']."'"));
                                        $posttask      = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tbl_posttask WHERE user='".$fetch['id']."'"));
                                    ?>
                                    <tr>
                                        <?php print '<td>'.$t.'</td>
                                        <td>'.$uplink['username'].' ('.$fetch['uplink'].')</td>
                                        <td>'.$fetch['username'].' ('.$fetch['id'].')</td>
                                        <td>'.$fetch['useragent'].'</td>
                                        <td>'.$fetch['device_id'].'</td>
                                        <td>'.$a.'</td>
                                        <td>'.$liketask.'</td>
                                        <td>'.$followtask.'</td>
                                        <td>'.$posttask.'</td>
                                        <td>'.$fetch['insertdate'].'</td>'; ?>
                                        <td>
                                            <span><a href="?delete=<?=$fetch['row']?>&uid=<?=$fetch['id']?>" onclick="return confirm('Yakin mau dihapus?')"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a></span>
                                        </td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example1').DataTable();
        } );
    </script>
    </body>
</html>