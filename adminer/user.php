<?php
require('../lib/config.php');
session_start();
include('../lib/akses.php');
if($data_user['level'] <> 'admin'){
    header("Location: ".$config['host']);
    exit;
}
if($_GET['delete']){
    if(!is_numeric($_GET['delete'])){
        header("Location: ".$config['host']."/adminer/user.php");
        exit;
    }
    $id  = trim_replace($_GET['delete']);
    $uid = trim_replace($_GET['delete']);
    $t   = mysqli_query($conn, "SELECT FROM tbl_members WHERE id='$id'");
    $ew  = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/adminer/user.php");
        exit;
    }
    mysqli_query($conn, "DELETE FROM tbl_instagram WHERE uplink='$id'");
    mysqli_query($conn, "DELETE FROM tbl_liketask WHERE uplink='$uid'");
    mysqli_query($conn, "DELETE FROM tbl_followtask WHERE uplink='$uid'");
    mysqli_query($conn, "DELETE FROM tbl_posttask WHERE uplink='$uid'");
    header("Location: ".$config['host']."/adminer/user.php");
}
?>
<!DOCTYPE html>
<html>
<?php include('../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../lib/sidebar.phtml'); ?>
        <?php include('../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Admin / Manage User Account</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Manage User Account</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <table id="example1" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Level</th>
                                        <th>Max Account</th>
                                        <th>Max Service</th>
                                        <th>Last Login</th>
                                        <th>Register Date</th>
                                        <th>Expired</th>
                                        <th>Uplink</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalmembers = mysqli_query($conn, "SELECT * FROM tbl_members");
                                    $t            = 0;
                                    $ew           = mysqli_num_rows($totalmembers);
                                    if($ew < 1){
                                        print '<tr><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td></tr>';
                                    } else {
                                      while($fetch = mysqli_fetch_array($totalmembers)){
                                        $t++;
                                    ?>
                                    <tr>
                                        <?php print '<td>'.$t.'</td>
                                        <td>'.$fetch['username'].'</td>
                                        <td>'.$fetch['email'].'</td>
                                        <td><span class="label label-info">'.ucfirst($fetch['level']).'</span></td>
                                        <td>'.$fetch['max'].'</td>
                                        <td>'.$fetch['max_service'].'</td>
                                        <td>'.$fetch['lastlog'].'</td>
                                        <td>'.$fetch['regdate'].'</td>
                                        <td>'.date('d/m/Y', $fetch['expired']).'</td>
                                        <td>'.$fetch['uplink'].'</td>'; ?>
                                        <td>
                                            <span><a href="?delete=<?=$fetch['id']?>&uid=<?=$fetch['id']?>" onclick="return confirm('Yakin mau dihapus?')"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a></span>
                                        </td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example1').DataTable();
        } );
    </script>
    </body>
</html>