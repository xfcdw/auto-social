<?php
require('lib/config.php');
session_start();
include('lib/akses.php');
?>
<!DOCTYPE html>
<html>
<?php include('lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('lib/sidebar.phtml'); ?>
        <?php include('lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['namev2'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Beranda</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="container">
                <div class="alert alert-info">
                <marquee><strong>Info!</strong> Durasi anda tersisa, <?php 
                $future = $data_user['expired'];
                $timefromdb = strtotime(date('d-m-Y'));
                $timeleft = $future-$timefromdb;
                $daysleft = round((($timeleft/24)/60)/60); 
                print $daysleft; ?> Hari Lagi</marquee></div>
            </div>
            <div class="col-lg-3">
                <table class="table table-responsive">
                     <tbody>
                      <tr>
                        <td><b>Username</b></td>
                        <td><b>:</b></td>
                        <td><?=$data_user['username'];?></td>
                      </tr>
                      <tr>
                        <td><b>Tanggal Registrasi</b></td>
                        <td><b>:</b></td>
                        <td><?=$data_user['regdate'];?>
                      </tr>
                      <tr>
                        <td><b>Masa Aktif s.d</b></td>
                        <td><b>:</b></td>
                        <td><?=date('d/m/Y H:i:s', $data_user['expired']);?></td>
                      </tr>
                      <tr>
                        <td><b>Terakhir Login</b></td>
                        <td><b>:</b></td>
                        <td><?=$data_user['lastlog'];?></td>
                      </tr>
                      <tr>
                        <td><b>IP Address</b></td>
                        <td><b>:</b></td>
                        <td><?=$_SERVER['REMOTE_ADDR'];?></td>
                      </tr>
                      <tr>
                        <td><b>Browser</b></td>
                        <td><b>:</b></td>
                        <td><?=$_SERVER['HTTP_USER_AGENT'];?></td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-shield fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> Level Akun </span>
                            <h2 class="font-bold"><?=ucfirst($data_user['level']);?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="widget lazur-bg p-xl">
                    <h2><?=strtoupper($data_user['username']);?></h2>
                    <ul class="list-unstyled m-t-md">
                        <li>
                            <span class="fa fa-envelope m-r-xs"></span>
                            <label>Email:</label>
                            <?=$data_user['email'];?>
                        </li>
                         <li>
                            <span class="fa fa-shield m-r-xs"></span>
                            <label>Level:</label>
                            <?=ucfirst($data_user['level']);?>
                        </li>
                    </ul>
                </div>
                <br><br>
            </div>
            
        </div>
        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>

    <?php include('lib/footer.phtml'); ?>
    </body>
</html>