<?php
require('../../lib/config.php');
session_start();
include('../../lib/akses.php');
?>
<!DOCTYPE html>
<html>
<?php include('../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../lib/sidebar.phtml'); ?>
        <?php include('../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Bot</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Info Tools</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="col-lg-12">
                                <div class="widget style1 red-bg">
                                    <div class="row">
                                        <div class="col-4">
                                            <i class="fa fa-instagram fa-5x"></i>
                                        </div>
                                        <div class="col-8 text-right">
                                            <span>Total Instagram Account </span>
                                            <h2 class="font-bold"><?=mysqli_num_rows(mysqli_query($conn, "SELECT id FROM tbl_instagram WHERE uplink='".$data_user['id']."'"));?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Daftar Tools</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Account Manager</td>
                                    <td> - </td>
                                    <td><a href="<?=$config['host'];?>/p/instabot/tools/account"><button class="btn btn-primary"><i class="fa fa-external-link"></i> Buka Tools</button></a></td>
                                </tr>
                                <tr>
                                    <td>Repost</td>
                                    <td> - </td>
                                    <td><a href="<?=$config['host'];?>/p/instapost/tools/repost"><button class="btn btn-primary"><i class="fa fa-external-link"></i> Buka Tools</button></a></td>
                                </tr>
                                <tr>
                                    <td>Schedule Repost</td>
                                    <td> - </td>
                                    <td><a href="<?=$config['host'];?>/p/instapost/tools/repost-schedule"><button class="btn btn-primary"><i class="fa fa-external-link"></i> Buka Tools</button></a></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../..//lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    </body>
</html>