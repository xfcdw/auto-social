<?php
require('../../../lib/config.php');
session_start();
include('../../../lib/akses.php');
if($_GET['delete']){
    if(!is_numeric($_GET['delete'])){
        header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
        exit;
    }
    $id = trim_replace($_GET['delete']);
    $t  = mysqli_query($conn, "SELECT uplink FROM tbl_posttask WHERE id='$id'");
    $ew = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
        exit;
    }
    mysqli_query($conn, "DELETE FROM tbl_posttask WHERE id='$id'");
    header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
} elseif($_GET['views']){
    if(!is_numeric($_GET['views'])){
        header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
        exit;
    }
    $id = trim_replace($_GET['views']);
    $t  = mysqli_query($conn, "SELECT * FROM tbl_tasklog WHERE task_id='$id'");
    $ew = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/p/instapost/tools/repost-schedule");
        exit;
    }
    while($logs = mysqli_fetch_array($t)){
        echo $logs['output'];
    }
} else {
?>
<!DOCTYPE html>
<html>
<?php include('../../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../../lib/sidebar.phtml'); ?>
        <?php include('../../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Post</strong>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Repost</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Repost</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form id="form1" action="act/api.php" method="post">
                              <input type="hidden" name="aksi" id="aksi" value="REPOST_JADWAL">
                              <div class="form-group">
                                <select class="form-control" name="user" id="user">
                                  <?php
                                  $sql = mysqli_query($conn, "SELECT id,username FROM tbl_instagram WHERE uplink='".$data_user['id']."'");
                                  while($fetch = mysqli_fetch_array($sql))
                                  print '<option value="'.$fetch['id'].'">@'.$fetch['username'].'</option>';
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <input type="text" name="target" id="target" class="form-control" placeholder="Post Url" required>
                              </div>
                              <div class="form-group">
                                <textarea id="caption" name="caption" class="form-control" placeholder="Caption" required></textarea>
                              </div>
                              <div class="form-group">
                                <input type="date" class="form-control" name="date" id="date" placeholder="Date" required/>
                              </div>
                              <div class="form-group">
                                <select class="form-control" name="hours" id="hours" required>
                                  <option value="">-- Hours --</option>
                                  <?php
                                  for ($i = 1; $i <= 24; $i++){
                                  print '<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'">'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>';
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <select class="form-control" name="minutes" id="minutes" required>
                                  <option value="">-- Minutes --</option>
                                  <?php
                                  for ($i = 1; $i <= 60; $i++){
                                  print '<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'">'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>';
                                  }
                                  ?>
                                </select>
                              </div>
                              <!-- Change this to a button or input when using this as a form -->
                              <button type="submit" name="submit" class="btn btn-primary btn-block">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Result Box</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="alert alert-info">Use <code>\n</code> for new line</div>
                            <div class="alert alert-info">Use <code>{user_source}</code> for mention own media source</div>
                            <div id="loading"></div>
                            <div id="kirim-ok"></div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>User</th>
                                            <th>Caption</th>
                                            <th>Target Post</th>
                                            <th>Status</th>
                                            <th>Time Exec</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $totalmembers = mysqli_query($conn, "SELECT * FROM tbl_posttask WHERE uplink='".$data_user['id']."'");
                                        $t            = 0;
                                        $ew           = mysqli_num_rows($totalmembers);
                                        if($ew<1){
                                          print '<tr><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td></tr>';
                                        } else {
                                          while($fetch = mysqli_fetch_array($totalmembers)){
                                            $t++;
                                            $dataakun  = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE id='".$fetch['user']."'");
                                            $dataakun  = mysqli_fetch_array($dataakun);
                                        ?>
                                        <tr>
                                            <?php print '<td>'.$t.'</td>
                                            <td>'.$fetch['user'].' ('.$dataakun['username'].')</td>
                                            <td>'.str_replace('\n', '<br>', base64_decode($fetch['caption'])).'</td>
                                            <td>'.$fetch['photoName'].'</td>
                                            <td>'.$fetch['status'].'</td>
                                            <td>'.date('Y-m-d H:i:s', $fetch['next_time_exec']).'</td>'; ?>
                                            <td>
                                                <span><a href="?delete=<?=$fetch['id']?>" onclick="return confirm('Yakin mau dihapus?')"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a></span>
                                                <span><a href="?views=<?=$fetch['id']?>"><button class="btn btn-sm btn-info"><i class="fa fa-external-link"></i></button></a></span>
                                            </td>
                                        </tr>
                                        <?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../../../lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    <script>
        $("#form1").submit(function(event){
            event.preventDefault();
            mulai();
            var $form = $(this), url = $form.attr('action');
            var posting = $.post(url, {
                aksi : $('#aksi').val(),
                user : $('#user').val(),
                target : $('#target').val(),
                caption : $('#caption').val(),
                date : $('#date').val(),
                hours : $('#hours').val(),
                minutes : $('#minutes').val()
            });

            posting.done(function(data) {
                selesai();
                $("#kirim-ok").append(data);
            });
        });
    </script>
    <script type="text/javascript">
        function mulai(){
            $('#kirim-ok').html("");
            $('#kirim-ok2').html("");
            $('#loading').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $("input").attr("disabled", "disabled");
            $("select").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
            $("textarea").attr("disabled", "disabled");
        }
        function selesai(){
            $('#loading').html("");
            $("input").removeAttr("disabled");
            $("select").removeAttr("disabled");
            $("button").removeAttr("disabled");
            $("textarea").removeAttr("disabled");
        }
    </script>
    </body>
</html>
<?php } ?>