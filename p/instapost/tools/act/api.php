<?php
require('../../../../lib/config.php');
session_start();
include('../../../../lib/akses.php');
// [---------------] REPOST PHOTO [---------------]
if($_POST['aksi'] == 'REPOST'){
    $id = trim_replace($_POST['user']);
    $sq = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE id='$id'");
    if(mysqli_num_rows($sq)<1)
        die('Undefined error');
    $f  = mysqli_fetch_array($sq);
    // get media
    $info   = proccess(1, $f['useragent'], 'media/'.getmediaid($_POST['target']).'/info/', $f['cookies']);
    $info   = json_decode($info[1], true);
    if($info['status'] == 'ok'){
        if($info['items'][0]['media_type'] == 1){
            $url          = $info['items'][0]['image_versions2']['candidates'][0]['url'];
            $contents     = file_get_contents($url);
            $imagename    = basename($url);
            $save_path    = "./tmp/".time().".jpg";
            file_put_contents($save_path, $contents);
            // uploading
            $file      = './tmp/'.$imagename.'';
            $photo     = curl_file_create(realpath($save_path));
            $upid      = number_format(round(microtime(true) * 1000), 0, '', '');
            $posts     = proccess(1, $f['useragent'], 'upload/photo/', $f['cookies'], array('upload_id' => $upid, 'image_compression' => '{"lib_name":"jt","lib_version":"1.3.0","quality":"100"}', 'photo' => $photo), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
            $post      = json_decode($posts[1]);
            $capt      = str_replace('{user_source}', '@'.$info['items'][0]['user']['username'].'', $_POST['caption']);
            $capt      = str_replace('{caption_ori}', $info['items'][0]['caption']['text'], $capt);
            $capt      = $capt;
            if($post->status==ok){
                $x       = proccess(1, $f['useragent'], 'media/configure/', $f['cookies'], hook('{"device_id":"'.$f['device_id'].'","guid":"'.generate_guid().'","upload_id":"'.$post->upload_id.'","caption":"'.$capt.'","device_timestamp":"'.time().'","source_type":"4","filter_type":"0","extra":"{}","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
                $xs     = json_decode($x[1]);
                if($xs->status == 'ok'){
                    echo "Upload success....";
                } else  {
                    echo "Failed";
                    echo json_encode($info);
                }
            } else {
                echo "Failed to Upload <br> ".var_dump($posts)." ";
            }
        } else if($info['items'][0]['media_type'] == 2){
            $url          = $info['items'][0]['video_versions'][0]['url'];
            $contents     = file_get_contents($url);
            $imagename    = basename($url);
            $save_path    = "./tmp/".time().".mp4";
            file_put_contents($save_path,$contents);
            $urlthumb     = $info['items'][0]['image_versions2']['candidates'][0]['url'];
            $contentsjpg  = file_get_contents($urlthumb);
            $thumb_path   = "./tmp/".time().".jpg";
            file_put_contents($thumb_path,$contentsjpg);
            $thumbnail    = curl_file_create(realpath($thumb_path));
            $upid         = number_format(round(microtime(true) * 1000), 0, '', '');
            $posts        = proccess(1, $f['useragent'], 'upload/video/', $f['cookies'], array('upload_id' => $upid, 'media_type' => 2), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
            $post         = json_decode($posts[1], true);
            $poststhumb   = proccess(1, $f['useragent'], 'upload/photo/', $f['cookies'], array('upload_id' => $upid, 'image_compression' => '{"lib_name":"jt","lib_version":"1.3.0","quality":"100"}', 'photo' => $thumbnail), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
            $postthumb    = json_decode($poststhumb[1]);
            $capt         = str_replace('{user_source}', '@'.$info['items'][0]['user']['username'].'', $_POST['caption']);
            $capt         = str_replace('{caption_ori}', $info['items'][0]['caption']['text'], $capt);
            $capt         = $capt;
            if($post['status'] == ok){
                $uploadUrl = $post['video_upload_urls'][3]['url'];
                $job       = $post['video_upload_urls'][3]['job'];
                $headers   = array();
                $headers[] = "Session-ID: ".$upid;
                $headers[] = "job: ".$job;
                $headers[] = "Content-Disposition: attachment; filename=\"".str_replace('./tmp/', '', $save_path)."\"";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$uploadUrl);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_USERAGENT, $f['useragent']);
                curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents(realpath($save_path)));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_COOKIE, $f['cookies']);
                $result = curl_exec($ch);
                $arrResult = json_decode($result, true);
                curl_close ($ch);
                if($arrResult['status'] == "ok"){
                    sleep(20);
                    $x       = proccess(1, $f['useragent'], 'media/configure/?video=1', $f['cookies'], hook('{"device_id":"'.$f['device_id'].'","guid":"'.generate_guid().'","upload_id":"'.$upid.'","caption":"'.trim(urldecode(str_replace('%0D%0A', '\n', urlencode($capt)))).'","device_timestamp":"'.time().'","source_type":"4","filter_type":"0","extra":"{}","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
                    $xs      = json_decode($x[1]);
                    if($xs->status == 'ok'){
                        echo "Upload success....";
                    } else  {
                        echo "Failed";
                    }
                } else {
                  print 'error';
                  echo $result;
                  exit;
                }
            } else {
                echo "Failed to Upload";
            }
        } else if($info['items'][0]['media_type'] == 8){
            /*
            $datainfo = $info['items'][0]['carousel_media'];
            foreach($datainfo as $lineinfo){
                $albumid = $lineinfo['id'];
                if($lineinfo['media_type'] == 1){
                    $url          = $lineinfo['image_versions2']['candidates'][0]['url'];
                    $contents     = file_get_contents($url);
                    $imagename    = basename($url);
                    $save_path    = "./tmp/".time().".jpg";
                    file_put_contents($save_path,$contents);
                    $file      = './tmp/'.$imagename.'';
                    $photo     = curl_file_create(realpath($save_path));
                    $upid      = number_format(round(microtime(true) * 1000), 0, '', '');
                    $posts     = proccess(1, $f['useragent'], 'upload/photo/', $f['cookies'], array('upload_id' => $upid, 'image_compression' => '{"lib_name":"jt","lib_version":"1.3.0","quality":"100"}', 'photo' => $photo), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
                    $post      = json_decode($posts[1]);
                    $capt      = $_POST['caption'];
                    if($post->status==ok){
                        $x       = proccess(1, $f['useragent'], 'media/configure/', $f['cookies'], hook('{"device_id":"'.$f['device_id'].'","guid":"'.generate_guid().'","upload_id":"'.$post->upload_id.'","caption":"'.trim(urldecode(str_replace('%0D%0A', '\n', urlencode($capt)))).'","device_timestamp":"'.time().'","source_type":"4","filter_type":"0","extra":"{}","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
                        $xs     = json_decode($x[1]);
                        if($xs->status == 'ok'){
                            echo "Upload success....<br>";
                        } else  {
                            echo "Failed [1]<br>";
                        }
                    } else {
                        echo "Failed to Upload <br> ".var_dump($posts)." ";
                    }
                } else if($lineinfo['media_type'] == 2){
                    $url          = $lineinfo['image_versions2']['candidates'][0]['url'];
                    $contents     = file_get_contents($url);
                    $imagename    = basename($url);
                    $save_path    = "./tmp/".time().".mp4";
                    file_put_contents($save_path,$contents);
                }
            }
            */
        }
    } else {
        echo "Error to Download Media";
    }
} else if($_POST['aksi'] == 'REPOST_JADWAL'){
    $id         = trim_replace($_POST['user']);
    $sq         = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE id='$id'");
    if(mysqli_num_rows($sq) < 1)
        die('Undefined error');
    $f          = mysqli_fetch_array($sq);
    $target     = $_POST['target'];
    $capt       = base64_encode($_POST['caption']);
    $hari       = $_POST['date'];
    $hari       = str_replace('/', '-', $hari);
    $hari       = date('Y-m-d', strtotime($hari));
    $jam        = $_POST['hours'];
    $menit      = $_POST['minutes'];
    $format_fix = $hari.' '.$jam.':'.$menit;
    $next_time  = strtotime($format_fix);
    $insert     = mysqli_query($conn, "INSERT INTO tbl_posttask (uplink, user, caption, photoName, status, next_time_exec) VALUES ('".$data_user['id']."', '".$_POST['user']."', '".$capt."', '".$target."', 'pending', '".$next_time."')") or die(mysqli_error($conn));
    if($insert){
        echo '<script>window.location.href = "'.$config['host'].'/p/instapost/tools/repost-schedule";</script>';
    }
}
?>