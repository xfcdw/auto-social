<?php
#!/usr/local/bin/php
require('../../../../lib/config.php');
$cur_time     = date("Y-m-d H:i");
$cur_time_str = strtotime($cur_time);
$task_nya     = $argv[1];
$select_task  = mysqli_query($conn, "SELECT * FROM tbl_posttask WHERE id='".$task_nya."'");
$count_task   = mysqli_num_rows($select_task);
$sekarang     = date("Y-m-d H:i");
while($fetch  = mysqli_fetch_array($select_task)){
    $update     = mysqli_query($conn, "UPDATE tbl_posttask SET next_time_exec='1' WHERE id='".$fetch['id']."'");
    echo $fetch['user']." Exec \n";
    $id         = trim_replace($fetch['user']);
    $sq         = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE id='$id'");
    if(mysqli_num_rows($sq) < 1)
        die('Undefined error');
    $f          = mysqli_fetch_array($sq);
    $useragent  = $g['useragent'];
    $cookie     = $g['cookies'];
    $target     = $fetch['photoName'];
    // get media
    $info   = proccess(1, $f['useragent'], 'media/'.getmediaid($target).'/info/', $f['cookies']);
    $info   = json_decode($info[1], true);
    if($info['status'] == 'ok'){
        if($info['items'][0]['media_type'] == 1){
            $url          = $info['items'][0]['image_versions2']['candidates'][0]['url'];
            $contents     = file_get_contents($url);
            $imagename    = basename($url);
            $save_path    = "./tmp/".time().".jpg";
            file_put_contents($save_path, $contents);
            // uploading
            $file      = './tmp/'.$imagename.'';
            $photo     = curl_file_create(realpath($save_path));
            $upid      = number_format(round(microtime(true) * 1000), 0, '', '');
            $posts     = proccess(1, $f['useragent'], 'upload/photo/', $f['cookies'], array('upload_id' => $upid, 'image_compression' => '{"lib_name":"jt","lib_version":"1.3.0","quality":"100"}', 'photo' => $photo), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
            $post      = json_decode($posts[1]);
            $capt      = str_replace('{user_source}', '@'.$info['items'][0]['user']['username'].'', base64_decode($fetch['caption']));
            $capt      = $capt;
            if($post->status==ok){
                $x       = proccess(1, $f['useragent'], 'media/configure/', $f['cookies'], hook('{"device_id":"'.$f['device_id'].'","guid":"'.generate_guid().'","upload_id":"'.$post->upload_id.'","caption":"'.$capt.'","device_timestamp":"'.time().'","source_type":"4","filter_type":"0","extra":"{}","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
                $xs     = json_decode($x[1]);
                if($xs->status == 'ok'){
                    echo "Upload success....";
                    $update     = mysqli_query($conn, "UPDATE tbl_posttask SET status='success' WHERE id='".$fetch['id']."'");
                } else  {
                    echo "Failed";
                }
            } else {
                echo "Failed";
            }
        } else if($info['items'][0]['media_type'] == 2){
            $url          = $info['items'][0]['video_versions'][0]['url'];
            $contents     = file_get_contents($url);
            $imagename    = basename($url);
            $save_path    = "./tmp/".time().".mp4";
            file_put_contents($save_path,$contents);
            $urlthumb     = $info['items'][0]['image_versions2']['candidates'][0]['url'];
            $contentsjpg  = file_get_contents($urlthumb);
            $thumb_path   = "./tmp/".time().".jpg";
            file_put_contents($thumb_path,$contentsjpg);
            $thumbnail    = curl_file_create(realpath($thumb_path));
            $upid         = number_format(round(microtime(true) * 1000), 0, '', '');
            $posts        = proccess(1, $f['useragent'], 'upload/video/', $f['cookies'], array('upload_id' => $upid, 'media_type' => 2), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
            $post         = json_decode($posts[1], true);
            $poststhumb   = proccess(1, $f['useragent'], 'upload/photo/', $f['cookies'], array('upload_id' => $upid, 'image_compression' => '{"lib_name":"jt","lib_version":"1.3.0","quality":"100"}', 'photo' => $thumbnail), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
            $postthumb    = json_decode($poststhumb[1]);
            if($post['status'] == ok){
                $uploadUrl = $post['video_upload_urls'][3]['url'];
                $job       = $post['video_upload_urls'][3]['job'];
                $headers   = array();
                $headers[] = "Session-ID: ".$upid;
                $headers[] = "job: ".$job;
                $headers[] = "Content-Disposition: attachment; filename=\"".str_replace('./tmp/', '', $save_path)."\"";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$uploadUrl);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_USERAGENT, $f['useragent']);
                curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents(realpath($save_path)));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_COOKIE, $f['cookies']);
                $result = curl_exec($ch);
                $arrResult = json_decode($result, true);
                curl_close ($ch);
                if($arrResult['status'] == "ok"){
                    sleep(10);
                    $x       = proccess(1, $f['useragent'], 'media/configure/?video=1', $f['cookies'], hook('{"device_id":"'.$f['device_id'].'","guid":"'.generate_guid().'","upload_id":"'.$upid.'","caption":"'.trim(urldecode(str_replace('%0D%0A', '\n', urlencode($capt)))).'","device_timestamp":"'.time().'","source_type":"4","filter_type":"0","extra":"{}","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
                    $xs      = json_decode($x[1]);
                    if($xs->status == 'ok'){
                        echo "Upload success....";
                        $update     = mysqli_query($conn, "UPDATE tbl_posttask SET status='success' WHERE id='".$fetch['id']."'");
                    } else  {
                        echo "Failed";
                    }
                } else {
                  echo "Failed";
                }
            } else {
                echo "Failed to Upload";
            }
        } else if($info['items'][0]['media_type'] == 8){
           
        }
    } else {
        echo "Error to Download Media";
    }
}
?>