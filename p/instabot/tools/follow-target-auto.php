<?php
require('../../../lib/config.php');
session_start();
include('../../../lib/akses.php');
if($_GET['refresh']){
    if(!is_numeric($_GET['refresh'])){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $id = trim_replace($_GET['refresh']);
    $t  = mysqli_query($conn, "SELECT uplink, delay FROM tbl_followtask WHERE id='$id'");
    $ew = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $cur_time   = date("Y-m-d H:i");
    $next_time  = strtotime( '+'.(int)$q['delay'].' minutes', strtotime($cur_time));
    $update     = mysqli_query($conn, "UPDATE tbl_followtask SET next_time_exec='".$next_time."' WHERE id='".$id."'");
    header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
} elseif($_GET['delete']){
    if(!is_numeric($_GET['delete'])){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $id = trim_replace($_GET['delete']);
    $t  = mysqli_query($conn, "SELECT uplink FROM tbl_followtask WHERE id='$id'");
    $ew = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    mysqli_query($conn, "DELETE FROM tbl_followtask WHERE id='$id'");
    header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
} elseif($_GET['views']){
    if(!is_numeric($_GET['views'])){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $id = trim_replace($_GET['views']);
    $t  = mysqli_query($conn, "SELECT * FROM tbl_tasklog WHERE task_id='$id'");
    $ew = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/p/instabot/tools/follow-target-auto");
        exit;
    }
    echo '<pre>';
    while($logs = mysqli_fetch_array($t)){
        echo $logs['output'];
    }
    echo '</pre>';
} else {
?>
<!DOCTYPE html>
<html>
<?php include('../../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../../lib/sidebar.phtml'); ?>
        <?php include('../../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Tools</strong>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Follow Target Automatic</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Follow Target Automatic</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form id="form1" action="act/api.php" method="post">
                              <input type="hidden" name="aksi" id="aksi" value="FOLLOW_TARGET_AUTO">
                              <div class="form-group">
                                <select class="form-control" name="user" id="user">
                                  <?php
                                  $sql = mysqli_query($conn, "SELECT id,username FROM tbl_instagram WHERE uplink='".$data_user['id']."'");
                                  while($fetch = mysqli_fetch_array($sql))
                                  print '<option value="'.$fetch['id'].'">@'.$fetch['username'].'</option>';
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <input class="form-control" placeholder="Username Target" id="username" name="username" type="text" required>
                              </div>
                              <div class="form-group">
                                <input class="form-control" placeholder="Input Total of Target You Want" id="ittyw" name="ittyw" type="number" required>
                              </div>
                              <div class="form-group">
                                <textarea class="form-control" placeholder="Input Text Comment (Use [|] if more than 1)" id="comment" name="comment"></textarea>
                              </div>
                              <div class="form-group">
                                <select class="form-control" id="tipe" name="tipe">
                                  <option value="followers">Follow Followers Target</option>
                                  <option value="following">Follow Following Target</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <input class="form-control" placeholder="Input Delay (Minutes)" id="delay" name="delay" type="number" required>
                              </div>
                              <!-- Change this to a button or input when using this as a form -->
                              <button type="submit" name="submit" class="btn btn-primary btn-block">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Result Box</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div id="loading"></div>
                            <div id="kirim-ok"></div>
                            <div class="table-responsive">
                                <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Target</th>
                                    <th>ITTYW</th>
                                    <th>Comment</th>
                                    <th>Tipe</th>
                                    <th>Delay</th>
                                    <th>Next Time Exec</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $totalmembers = mysqli_query($conn, "SELECT * FROM tbl_followtask WHERE uplink='".$data_user['id']."'");
                                $t            = 0;
                                $ew           = mysqli_num_rows($totalmembers);
                                if($ew<1){
                                  print '<tr><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td><td>Unavailable</td></tr>';
                                } else {
                                  while($fetch = mysqli_fetch_array($totalmembers)){
                                    $t++;
                                    $dataakun  = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE id='".$fetch['user']."'");
                                    $dataakun  = mysqli_fetch_array($dataakun);
                                ?>
                                <tr>
                                    <?php print '<td>'.$t.'</td>
                                    <td>'.$fetch['user'].' ('.$dataakun['username'].')</td>
                                    <td>'.$fetch['target'].'</td>
                                    <td>'.$fetch['ittyw'].'</td>
                                    <td>'.$fetch['comment'].'</td>
                                    <td>'.$fetch['tipe'].'</td>
                                    <td>'.$fetch['delay'].' Minutes</td>
                                    <td>'.date('Y-m-d H:i:s', $fetch['next_time_exec']).'</td>'; ?>
                                    <td>
                                        <span><a href="?refresh=<?=$fetch['id']?>"><button class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i></button></a></span><br><br>
                                        <span><a href="?views=<?=$fetch['id']?>"><button class="btn btn-sm btn-info"><i class="fa fa-external-link"></i></button></a></span><br><br>
                                        <span><a href="?delete=<?=$fetch['id']?>" onclick="return confirm('Yakin mau dihapus?')"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a></span>
                                    </td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../../../lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    <script>
        $("#form1").submit(function(event){
            event.preventDefault();
            mulai();
            var $form = $(this), url = $form.attr('action');
            var posting = $.post(url, {
                aksi : $('#aksi').val(),
                user : $('#user').val(),
                username : $('#username').val(),
                ittyw : $('#ittyw').val(),
                comment : $('#comment').val(),
                tipe : $('#tipe').val(),
                delay : $('#delay').val()
            });

            posting.done(function(data) {
                selesai();
                $("#kirim-ok").append(data);
            });
        });
    </script>
    <script type="text/javascript">
        function mulai(){
            $('#kirim-ok').html("");
            $('#kirim-ok2').html("");
            $('#loading').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $("input").attr("disabled", "disabled");
            $("select").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
            $("textarea").attr("disabled", "disabled");
        }
        function selesai(){
            $('#loading').html("");
            $("input").removeAttr("disabled");
            $("select").removeAttr("disabled");
            $("button").removeAttr("disabled");
            $("textarea").removeAttr("disabled");
        }
    </script>
    </body>
</html>
<?php } ?>