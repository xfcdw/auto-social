<?php
require('../../../lib/config.php');
session_start();
include('../../../lib/akses.php');
?>
<!DOCTYPE html>
<html>
<?php include('../../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../../lib/sidebar.phtml'); ?>
        <?php include('../../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Tools</strong>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Collections</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Collections</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form id="form1" action="act/api.php" method="post">
                              <input type="hidden" name="aksi" id="aksi" value="GET_COLLECTIONS">
                              <div class="form-group">
                                <select class="form-control" name="user" id="user">
                                  <?php
                                  $sql = mysqli_query($conn, "SELECT id,username FROM tbl_instagram WHERE uplink='".$data_user['id']."'");
                                  while($fetch = mysqli_fetch_array($sql))
                                  print '<option value="'.$fetch['id'].'">@'.$fetch['username'].'</option>';
                                  ?>
                                </select>
                              </div>
                              <!-- Change this to a button or input when using this as a form -->
                              <button type="submit" name="submit" class="btn btn-primary btn-block">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Result Box</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div id="loading"></div>
                            <div id="loading2"></div>
                            <div id="kirim-ok"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../../../lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    <script>
        $("#form1").submit(function(event){
            event.preventDefault();
            mulai();
            var $form = $(this), url = $form.attr('action');
            var posting = $.post(url, {
                aksi : $('#aksi').val(),
                user : $('#user').val()
            });

            posting.done(function(data) {
                selesai();
                $("#kirim-ok").append(data);
            });
        });
    </script>
    <script type="text/javascript">
        function saveCollections(user, id, name){
            event.preventDefault();
            mulai();
            $('#loading2').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $.get("act/api.php?aksi=DOWNLOAD_COLLECTIONS_V2&user="+ user +"&id="+ id +"&name="+ name +"", function(data, status){
                $("#kirim-ok").append(data);
                $('#loading2').html("");
                selesai();
            });
        }
    </script>
    <script type="text/javascript">
        function mulai(){
            $('#kirim-ok').html("");
            $('#kirim-ok2').html("");
            $('#loading').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $("input").attr("disabled", "disabled");
            $("select").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
            $("textarea").attr("disabled", "disabled");
        }
        function selesai(){
            $('#loading').html("");
            $("input").removeAttr("disabled");
            $("select").removeAttr("disabled");
            $("button").removeAttr("disabled");
            $("textarea").removeAttr("disabled");
        }
    </script>
    </body>
</html>