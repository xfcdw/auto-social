<?php
require('../../../lib/config.php');
session_start();
include('../../../lib/akses.php');
?>
<!DOCTYPE html>
<html>
<?php include('../../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../../lib/sidebar.phtml'); ?>
        <?php include('../../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Tools</strong>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Panel Story Views</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Panel Story Views</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <?php
                            $dataserver = file_get_contents('https://web-nthanfp.c9users.io/x.php');
                            if($dataserver){
                                $server = 'OK';
                            } else {
                                $server = 'SERVER DOWN';
                            }
                            ?>
                            <b> Server Status   : </b><?php echo $server; ?><br>
                            <b> Jumlah Database : </b><?php echo file_get_contents('https://bot.nthanfp.me/instagram/api-jumlah.php'); ?><br><br>
                            <form id="form1" action="act/api.php" method="post">
                              <input type="hidden" name="aksi" id="aksi" value="PANEL_STORY">
                              <div class="form-group">
                                <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                              </div>
                              <div class="form-group">
                                <input type="number" name="number" id="number" class="form-control" placeholder="Story Number (start from 1)">
                              </div>
                              <!-- Change this to a button or input when using this as a form -->
                              <button type="submit" name="submit" class="btn btn-primary btn-block">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Result Box</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div id="loading"></div>
                            <div id="kirim-ok"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../../../lib/footer.phtml'); ?>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    <script>
        $("#form1").submit(function(event){
            event.preventDefault();
            mulai();
            var $form = $(this), url = $form.attr('action');
            var posting = $.post(url, {
                aksi : $('#aksi').val(),
                username : $('#username').val(),
                number : $('#number').val()
            });

            posting.done(function(data) {
                selesai();
                $("#kirim-ok").append(data);
            });
        });
    </script>
    <script type="text/javascript">
        function mulai(){
            $('#kirim-ok').html("");
            $('#kirim-ok2').html("");
            $('#loading').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $("input").attr("disabled", "disabled");
            $("select").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
            $("textarea").attr("disabled", "disabled");
        }
        function selesai(){
            $('#loading').html("");
            $("input").removeAttr("disabled");
            $("select").removeAttr("disabled");
            $("button").removeAttr("disabled");
            $("textarea").removeAttr("disabled");
        }
    </script>
    </body>
</html>