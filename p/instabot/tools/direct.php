<?php
require('../../../lib/config.php');
session_start();
include('../../../lib/akses.php');
?>
<!DOCTYPE html>
<html>
<?php include('../../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../../lib/sidebar.phtml'); ?>
        <?php include('../../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Tools</strong>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Direct Message</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
        
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox chat-view">
                        <div class="ibox-title">
                            <small class="float-right text-muted">Last message:  Mon Jan 26 2015 - 18:39:23</small>
                             DM Panels
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                
                                <div class="col-md-9">
                                    <div class="chat-discussion" id="kirim-ok2">
                                        <div id="loading2"></div>
                                        <center><div class="alert alert-info">Nothing</div></center>
                                    </div>
                                    <div class="chat-message-form">
                                        <div class="form-group">
                                            <textarea class="form-control message-input" name="message" placeholder="Enter message text"></textarea>
                                        </div>
                                    </div>
                                    <br><br>
                                </div>
                                
                                <div class="col-md-3">
                                    <?php
                                    if(empty($_GET['user'])){
                                    ?>
                                    <br><br>
                                    <div class="container">
                                        <form action="" method="GET">
                                            <div class="form-group">
                                                <label>Select User</label>
                                                <select class="form-control" name="user" id="user">
                                                  <?php
                                                  $sql = mysqli_query($conn, "SELECT id,username FROM tbl_instagram WHERE uplink='".$data_user['id']."'");
                                                  while($fetch = mysqli_fetch_array($sql))
                                                  print '<option value="'.$fetch['id'].'">@'.$fetch['username'].'</option>';
                                                  ?>
                                                </select>
                                            </div>
                                          <button type="submit" name="submit" class="btn btn-primary btn-block">SUBMIT</button>
                                        </form>
                                    </div>
                                    <?php } ?>
                                    <div class="chat-users">
                                        <div class="users-list">
                                            <?php
                                            if(!empty($_GET['user'])){
                                                $id         = trim_replace($_GET['user']);
                                                $sq         = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE id='$id'");
                                                if(mysqli_num_rows($sq)<1)
                                                    die('Undefined error');
                                                $fa         = mysqli_fetch_array($sq);
                                                $inboxs     = proccess(1, $fa['useragent'], 'direct_v2/inbox/', $fa['cookies']);
                                                $inbox      = json_decode($inboxs[1], true);
                                                $datas      = $inbox['inbox']['threads'];
                                                foreach($datas as $data){
                                            ?>
                                            
                                            <div class="chat-user" onclick="getThread('<?=$_GET['user'];?>', '<?=$data['thread_id'];?>')">
                                                <img class="chat-avatar" src="<?=$data['users'][0]['profile_pic_url'];?>" alt="">
                                                <div class="chat-user-name">
                                                    <span><?=$data['users'][0]['username'];?></span>
                                                </div>
                                            </div>
                                            
                                            <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
        
            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../../../lib/footer.phtml'); ?>
    <script type="text/javascript">
        function getThread(user, id){
            event.preventDefault();
            mulai();
            $('#loading2').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $.get("act/api.php?aksi=VIEWS_THREADS_USER&user="+ user +"&id="+ id +"", function(data, status){
                $("#kirim-ok2").append(data);
                $('#loading2').html("");
                selesai();
            });
        }
    </script>
    <script>
        $("#form1").submit(function(event){
            event.preventDefault();
            mulai();
            var $form = $(this), url = $form.attr('action');
            var posting = $.post(url, {
                aksi : $('#aksi').val(),
                user : $('#user').val(),
                username : $('#username').val()
            });

            posting.done(function(data) {
                selesai();
                $("#kirim-ok").append(data);
            });
        });
    </script>
    <script type="text/javascript">
        function mulai(){
            $('#kirim-ok').html("");
            $('#kirim-ok2').html("");
            $('#loading').html('<img src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/loading.gif">');
            $("input").attr("disabled", "disabled");
            $("select").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
            $("textarea").attr("disabled", "disabled");
        }
        function selesai(){
            $('#loading').html("");
            $("input").removeAttr("disabled");
            $("select").removeAttr("disabled");
            $("button").removeAttr("disabled");
            $("textarea").removeAttr("disabled");
        }
    </script>
    </body>
</html>