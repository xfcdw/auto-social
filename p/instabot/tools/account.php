<?php
require('../../../lib/config.php');
session_start();
include('../../../lib/akses.php');
if($_POST['aksi'] == 'TAMBAH_AKUN'){
    $useragent = generate_useragent();
    $device_id = generate_device_id();
    $user      = $_POST['username'];
    $pass      = $_POST['password'];
    $uidd      = $data_user['id'];
    $cek       = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE username='".$user."'") or die(mysqli_error($conn).' 1');
    //$aq        = mysqli_query($conn, "SELECT FROM tbl_members WHERE username='".$uidd."'") or die(mysqli_error($conn).' 2');
	//$qw        = mysqli_fetch_assoc($aq);
	$totalig   = mysqli_query($conn, "SELECT id FROM tbl_instagram WHERE uplink='".$data_user['id']."'") or die(mysqli_error($conn).' 3');
    $ew        = mysqli_num_rows($totalig);
    if($ew >= $data_user['max']){
        $output  = array('status' => 'error', 'msg' => 'Anda tidak dapat menambah user lagi..');
    } else if(mysqli_num_rows($cek) > 0){
        $output  = array('status' => 'error', 'msg' => 'User sudah terdaftar');
    } else {
        $login = proccess(1, $useragent, 'accounts/login/', 0, hook('{"device_id":"' . $device_id . '","guid":"' . generate_guid() . '","username":"'.$user.'","password":"'.$pass.'","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
        $ext   = json_decode($login[1]);
        if ($ext->status <> ok) {
            $output  = array('status' => 'error', 'msg' => $ext->message);
        } else {
            preg_match_all('%Set-Cookie: (.*?);%', $login[0], $d);
            $cookie = '';
            for ($o = 0; $o < count($d[0]); $o++)
                $cookie .= $d[1][$o] . ";";
            $uname = $ext->logged_in_user->username;
            $req = proccess(1, $useragent, 'feed/timeline/', $cookie);
            $is_verified = (json_decode($req[1])->status<>ok) ? 0 : 1;
            $uid = $ext->logged_in_user->pk;
            mysqli_query($conn, "INSERT INTO tbl_instagram (id, cookies, useragent, device_id, verifikasi, insertdate, username, uplink) values ('$uid', '$cookie', '$useragent', '$device_id', $is_verified, '".date('d/m/Y H:i:s')."', '$uname', '".$data_user['id']."')") or die(mysqli_error($conn));
            $output      = array('status' => 'ok', 'msg' => 'User berhasil ditambahkan...');
        }
    }
} elseif($_POST['aksi'] == 'TAMBAH_AKUN_COOKIE'){
    $useragent = generate_useragent();
    $device_id = generate_device_id();
    $cookie    = $_POST['cookie'];
    $uidd      = $data_user['id'];
    //$cek       = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE username='".$user."'") or die(mysqli_error($conn).' 1');
    //$aq        = mysqli_query($conn, "SELECT FROM tbl_members WHERE username='".$uidd."'") or die(mysqli_error($conn).' 2');
	//$qw        = mysqli_fetch_assoc($aq);
	$totalig   = mysqli_query($conn, "SELECT id FROM tbl_instagram WHERE uplink='".$data_user['id']."'") or die(mysqli_error($conn).' 3');
    $ew        = mysqli_num_rows($totalig);
    if($ew >= $data_user['max']){
        $output  = array('status' => 'error', 'msg' => 'Anda tidak dapat menambah user lagi..');
    } else {
        $login = proccess(1, $useragent, 'accounts/current_user/', $cookie);
        //$login = proccess(1, $useragent, 'accounts/login/', 0, hook('{"device_id":"' . $device_id . '","guid":"' . generate_guid() . '","username":"'.$user.'","password":"'.$pass.'","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
        $ext   = json_decode($login[1]);
        if ($ext->status <> ok) {
            $output  = array('status' => 'error', 'msg' => str_replace('login_required', 'Invalid Cookie', $ext->message));
        } else {
            $cek         = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE username='".$user."'") or die(mysqli_error($conn).' 1');
            if(mysqli_num_rows($cek) > 0){
                $output  = array('status' => 'error', 'msg' => 'User sudah terdaftar');
            } else {
                $cookie      = $_POST['cookie'];
                $uname       = $ext->user->username;
                $req         = proccess(1, $useragent, 'feed/timeline/', $cookie);
                $is_verified = (json_decode($req[1])->status<>ok) ? 0 : 1;
                $uid         = $ext->user->pk;
                mysqli_query($conn, "INSERT INTO tbl_instagram (id, cookies, useragent, device_id, verifikasi, insertdate, username, uplink) values ('$uid', '$cookie', '$useragent', '$device_id', $is_verified, '".date('d/m/Y H:i:s')."', '$uname', '".$data_user['id']."')") or die(mysqli_error($conn));
                $output      = array('status' => 'ok', 'msg' => 'User berhasil ditambahkan...');
            }
        }
    }
}
if($_GET['delete']){
    if(!is_numeric($_GET['delete'])){
        header("Location: ".$config['host']."/p/instabot/tools/account");
        exit;
    }
    $id = trim_replace($_GET['delete']);
    $t  = mysqli_query($conn, "SELECT uplink FROM tbl_instagram WHERE row='$id'");
    $ew = mysqli_num_rows($t);
    if($ew < 1){
        header("Location: ".$config['host']."/p/instabot/tools/account");
        exit;
    }
    $q  = mysqli_fetch_array($t);
    if($q['uplink']<>$data_user['id']){
        header("Location: ".$config['host']."/p/instabot/tools/account");
        exit;
    }
    mysqli_query($conn, "DELETE FROM tbl_instagram WHERE row='$id'");
    header("Location: ".$config['host']."/p/instabot/tools/account");
}
?>
<!DOCTYPE html>
<html>
<?php include('../../../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../../../lib/sidebar.phtml'); ?>
        <?php include('../../../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Instagram Tools</strong>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Account Manager</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Tambah Akun</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <?php
                            if($output['status'] == 'ok'){
                                ?>
                                <div class="alert alert-success"><?=$output['msg'];?></div>
                                <?php
                            } elseif($output['status'] == 'error') {
                                ?>
                                <div class="alert alert-danger"><?=$output['msg'];?></div>
                                <?php
                            }
                            ?>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">With Password</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">With Cookies</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">With Facebook</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <br>
                                    <form action="" method="POST">
                                      <input type="hidden" name="aksi" value="TAMBAH_AKUN">
                                      <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username" placeholder="Username">
                                      </div>
                                      <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                      </div>
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <br>
                                    <form action="" method="POST">
                                      <input type="hidden" name="aksi" value="TAMBAH_AKUN_COOKIE">
                                      <div class="form-group">
                                        <label>Cookie</label>
                                        <textarea class="form-control" name="cookie" placeholder="Input Cookie"></textarea>
                                      </div>
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <br>
                                    <div class="alert alert-info">Coming Soon</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Daftar Akun</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Identity</th>
                                            <th>User Agent</th>
                                            <th>Device ID</th>
                                            <th>is Verified</th>
                                            <th>Insert Date</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $totalmembers = mysqli_query($conn, "SELECT * FROM tbl_instagram WHERE uplink='".$data_user['id']."'");
                                        $t            = 0;
                                        $ew           = mysqli_num_rows($totalmembers);
                                        if($ew<1){
                                          print '
                                          <tr>
                                              <td>Unavailable</td>
                                              <td>Unavailable</td>
                                              <td>Unavailable</td>
                                              <td>Unavailable</td>
                                              <td>Unavailable</td>
                                              <td>Unavailable</td>
                                              <td>Unavailable</td>
                                          </tr>';
                                        } else {
                                          while($fetch = mysqli_fetch_array($totalmembers)){
                                            $t++;
                                            if($fetch['verifikasi']<1){
                                              $a           = '<font color="orange">False</font>';
                                              $req         = proccess(1, $fetch['useragent'], 'feed/timeline/', $fetch['cookies']);
                                              $is_verified = (json_decode($req[1])->status<>ok) ? 0 : 1;
                                              mysqli_query($conn, "UPDATE tbl_instagram SET verifikasi=$is_verified WHERE row=$fetch[row]");
                                            } else {
                                              $a = '<font color="green">True</font>';
                                            }
                                        ?>
                                        <tr>
                                            <?php print '<td>'.$t.'</td>
                                            <td>'.$fetch['username'].' ('.$fetch['id'].')</td>
                                            <td>'.$fetch['useragent'].'</td>
                                            <td>'.$fetch['device_id'].'</td>
                                            <td>'.$a.'</td>
                                            <td>'.$fetch['insertdate'].'</td>'; ?>
                                            <td><a href="?delete=<?=$fetch['row']?>" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>
                                        </tr>
                                        <?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../../../lib/footer.phtml'); ?>
    </body>
</html>