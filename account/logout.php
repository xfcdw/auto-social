<?php
require('../lib/config.php');
session_start();
session_destroy();
if(isset($_COOKIE['username'])){
	$time = time();
	setcookie('username', $time - 3600);
	setcookie('password', $time - 3600);
}
header("Location: ".$config['host']."/account/login.php");
exit();