<?php
require('../lib/config.php');
session_start();
if($_COOKIE['username']&&$_COOKIE['password']){
    $username  = mysqli_real_escape_string($conn, $_COOKIE['username']);
    $password  = mysqli_real_escape_string($conn, $_COOKIE['password']);

    $getdata   = mysqli_query($conn, "SELECT * FROM tbl_members WHERE username='".$username."'");
    $data_user = mysqli_fetch_array($getdata);
    $date      = date('Y-m-d');
    $tgl       = strtotime('-1 day' , strtotime($date));
    if(mysqli_num_rows($getdata) < 1){
        $output    = array('status' => 'error_hide', 'msg' => 'Akun tidak di temukan');
    } else if($data_user['expired'] < $tgl){
        $output    = array('status' => 'error_hide', 'msg' => 'Masa aktif akun telah habis');
    } else if(password_verify($password, $data_user['password'])){
        $_SESSION['user_axepay'] = $data_user;
        $log                     = date('d/m/Y H:i:s');
        mysqli_query($conn, "UPDATE tbl_members SET lastlog='".$log."' WHERE username='".$username."'");
        header("Location: ".$config['host']);
    }
}
if($_POST['username']&&$_POST['password']){
    $username  = mysqli_real_escape_string($conn, $_POST['username']);
    $password  = mysqli_real_escape_string($conn, $_POST['password']);

    $getdata   = mysqli_query($conn, "SELECT * FROM tbl_members WHERE username='".$username."'");
    $data_user = mysqli_fetch_array($getdata);
    $date      = date('Y-m-d');
    $tgl       = strtotime('-1 day' , strtotime($date));
    if(mysqli_num_rows($getdata) < 1){
        $output    = array('status' => 'error', 'msg' => 'Akun tidak di temukan');
    } else if($data_user['expired'] < $tgl){
        $output    = array('status' => 'error', 'msg' => 'Masa aktif akun telah habis');
    } else if(password_verify($password, $data_user['password'])){
        $_SESSION['user_axepay'] = $data_user;
        $time = time();
        if(isset($_POST['remember'])){
            $time = time();
            setcookie('username', $username, $time + 2629746);//set cookie
            setcookie('password', $password, $time + 2629746);//set cookie
        }
        $log     = date('d/m/Y H:i:s');
        $output  = array('status' => 'ok', 'msg' => 'Login Berhasil');
        mysqli_query($conn, "UPDATE tbl_members SET lastlog='".$log."' WHERE username='".$username."'");
        header("Location: ".$config['host']);
    } else {
        $output  = array('status' => 'error', 'msg' => 'Username dan Password tidak cocok');
    }
}
?>
<!DOCTYPE html>
<html>
<?php include('../lib/header.phtml'); ?>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <br><br><br>
                <img class="img-fluid" src="<?=$config['host'];?>/assets/img/main-logo.jpg"></img>
            </div>
            <h3>Masuk untuk <?=$config['namev2'];?></h3>
            <?php
            if($output['status'] == 'ok'){
                ?>
                <div class="alert alert-success"><?=$output['msg'];?></div>
                <?php
            } elseif($output['status'] == 'error') {
                ?>
                <div class="alert alert-danger"><?=$output['msg'];?></div>
                <?php
            }
            ?>
            <form class="m-t" role="form" action="" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required="">
                </div>
                <div class="form-group text-left">
                    <div class="checkbox i-checks"><label> <input type="checkbox" name="remember"><i></i> Ingat saya</label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Masuk</button>

                <p class="text-muted text-center"><small>Belum mempunyai akun?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?=$config['host'];?>/account/register">Daftar</a>
            </form>
            <p class="m-t"> <small>Digital payment</small> </p>
        </div>
    </div>
    <?php include('../lib/footer.phtml'); ?>
    </body>
</html>