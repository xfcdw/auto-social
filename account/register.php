<?php
require('../lib/config.php');
session_start();
if($_POST['username']&&$_POST['email']&&$_POST['password']){
    $secret_key   = $recaptcha['secret'];
    $verify       = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']));
    if($verify->success){
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $email    = mysqli_real_escape_string($conn, $_POST['email']);
        $password = password_hash(mysqli_real_escape_string($conn, $_POST['password']), PASSWORD_DEFAULT);
        $durasi   = '7';
        $max_acc  = '3';
        $max_svc  = '1000';
        $lv       = 'member';
        $newdate  = strtotime ( '+'.(int)$durasi.' day' , strtotime ( $date ) ) ;
        $cekuser  = mysqli_query($conn, "SELECT * FROM tbl_members WHERE username = '".$username."'");
        if(mysqli_num_rows($cekuser) > 0){
            $output = array('status' => 'error', 'msg' => 'Email telah digunakan!');
        } else if (!preg_match('/^[a-zA-Z0-9-]+$/u', $username)){
            $output = array('status' => 'error', 'msg' => 'Username yang anda masukkan mengandung karakter terlarang, Anda dapat mengisi dengan angka ataupun huruf!');
        } else if (!preg_match('/^[a-zA-Z0-9-]+$/u', $_POST['password'])){
            $output = array('status' => 'error', 'msg' => 'Kata Sandi yang anda masukkan mengandung karakter terlarang, Anda dapat mengisi dengan angka ataupun huruf!');
        } else {
            $insert  = mysqli_query($conn, "INSERT INTO tbl_members (username, password, email, max, level, regdate, uplink, expired, max_service) values ('".$username."', '".$password."', '".$email."', '".(int)trim($max_acc)."', '".$lv."', '".date('d/m/Y H:i:s')."', 'server', '".$newdate."', ".(int)trim($max_svc).")");
            //$insert  = mysqli_query($conn, "INSERT INTO `tbl_customer` (email, name, phone, password, balance, pin, level, status, registered) VALUES ('".$email."', '".$name."' , '".$phone."', '".$password."', '15000', '', 'bronze', 'active', '".$today."')");
            if($insert){
                $body    = '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width" /><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Welcome to AxePay</title><style type="text/css">*{margin:0;padding:0;font-family:"Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;box-sizing:border-box;font-size:14px}img{max-width:100%}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100% !important;height:100%;line-height:1.6}table td{vertical-align:top}body{background-color:#f6f6f6}.body-wrap{background-color:#f6f6f6;width:100%}.container{display:block !important;max-width:600px !important;margin:0 auto !important;clear:both !important}.content{max-width:600px;margin:0 auto;display:block;padding:20px}.main{background:#fff;border:1px solid #e9e9e9;border-radius:3px}.content-wrap{padding:20px}.content-block{padding:0 0 20px}.header{width:100%;margin-bottom:20px}.footer{width:100%;clear:both;color:#999;padding:20px}.footer a{color:#999}.footer p, .footer a, .footer unsubscribe, .footer td{font-size:12px}h1,h2,h3{font-family:"Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;color:#000;margin:40px 0 0;line-height:1.2;font-weight:400}h1{font-size:32px;font-weight:500}h2{font-size:24px}h3{font-size:18px}h4{font-size:14px;font-weight:600}p,ul,ol{margin-bottom:10px;font-weight:normal}p li, ul li, ol li{margin-left:5px;list-style-position:inside}a{color:#1ab394;text-decoration:underline}.btn-primary{text-decoration:none;color:#FFF;background-color:#1ab394;border:solid #1ab394;border-width:5px 10px;line-height:2;font-weight:bold;text-align:center;cursor:pointer;display:inline-block;border-radius:5px;text-transform:capitalize}.last{margin-bottom:0}.first{margin-top:0}.aligncenter{text-align:center}.alignright{text-align:right}.alignleft{text-align:left}.clear{clear:both}.alert{font-size:16px;color:#fff;font-weight:500;padding:20px;text-align:center;border-radius:3px 3px 0 0}.alert a{color:#fff;text-decoration:none;font-weight:500;font-size:16px}.alert.alert-warning{background:#f8ac59}.alert.alert-bad{background:#ed5565}.alert.alert-good{background:#1ab394}.invoice{margin:40px auto;text-align:left;width:80%}.invoice td{padding:5px 0}.invoice .invoice-items{width:100%}.invoice .invoice-items td{border-top:#eee 1px solid}.invoice .invoice-items .total td{border-top:2px solid #333;border-bottom:2px solid #333;font-weight:700}@media only screen and (max-width: 640px){h1,h2,h3,h4{font-weight:600 !important;margin:20px 0 5px !important}h1{font-size:22px !important}h2{font-size:18px !important}h3{font-size:16px !important}.container{width:100% !important}.content,.content-wrap{padding:10px !important}.invoice{width:100% !important}}</style></head><body><table class="body-wrap"><tr><td></td><td class="container" width="600"><div class="content"><table class="main" width="100%" cellpadding="0" cellspacing="0"><tr><td class="content-wrap"><table cellpadding="0" cellspacing="0"><tr><td class="content-block"><h3>Terimakasih Telah Mendaftarkan Akun di AxePay</h3></td></tr><tr><td class="content-block"> Axe Payment (AxePay) adalah sebuah platform pembayaran untuk berbagai layanan social media marketing & distributor layanan pulsa yang bergerak terutama di Indonesia.<br />Dengan bergabung bersama kami, Anda dapat menjadi penyedia jasa social media atau reseller social media seperti jasa penambah Followers, Likes, dll & menjadi agen distributor pulsa seperti pulsa, paket internet, voucher game, token pln, dll.<br />Saat ini tersedia berbagai layanan untuk social media terpopuler seperti Instagram, Facebook, Twitter, Youtube, dll. & layanan pulsa, paket internet, voucher game,token pln, dll.</td></tr><tr><td class="content-block"> Silahkan konfirmasi melalui email anda dengan cara klik tombol di bawah ini</td></tr><tr><td class="content-block aligncenter"> <a href="#" class="btn-primary">Confirm email address</a></td></tr></table></td></tr></table><div class="footer"><table width="100%"><tr><td class="aligncenter content-block">Follow <a href="#">@AxePay</a> on Twitter.</td></tr></table></div></div></td><td></td></tr></table></body></html>';
                axemail('NTHANFP150503', $email, 'Axe Payment', 'no-reply@payment.nthanfp.me', 'Thanks You For Register AxePay!', $body, true);
                $output  = array('status' => 'ok', 'msg' => 'Pendaftaran sukses, Silahkan masuk dengan akun anda');
            } else {
                $output  = array('status' => 'error', 'msg' => 'Pendaftaran gagal, Silahkan kontak customer support');
            }
        }
    } else {
        $output  = array('status' => 'error', 'msg' => 'Wrong Captcha!');
    }
}
?>
<!DOCTYPE html>
<html>
<?php include('../lib/header.phtml'); ?>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <br><br><br>
                <img class="img-fluid" src="<?=$config['host'];?>/assets/img/main-logo.jpg"></img>
            </div>
            <h3>Daftar untuk <?=$config['namev2'];?></h3>
            <?php
            if($output['status'] == 'ok'){
                ?>
                <div class="alert alert-success"><?=$output['msg'];?></div>
                <?php
            } elseif($output['status'] == 'error') {
                ?>
                <div class="alert alert-danger"><?=$output['msg'];?></div>
                <?php
            }
            ?>
            <form class="m-t" role="form" action="" method="POST">
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="E-mail" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required="">
                </div>
                <div class="form-group text-left">
                    <div class="g-recaptcha" data-sitekey="<?=$recaptcha['sitekey'];?>"></div>
                </div>
                <div class="form-group text-left">
                    <div class="checkbox i-checks"><label><input type="checkbox" required><i></i> Setuju dengan ketentuan yang berlaku</label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Daftar</button>

                <p class="text-muted text-center"><small>Sudah mempunyai akun?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?=$config['host'];?>/account/login">Masuk</a>
            </form>
            <p class="m-t"> <small>Digital payment</small> </p>
        </div>
    </div>
    <?php include('../lib/footer.phtml'); ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
</html>