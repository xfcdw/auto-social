<?php
require('../lib/config.php');
session_start();
include('../lib/akses.php');
if($_POST['aksi'] == 'UPDATE_PROFILE'){
    $name   = mysqli_real_escape_string($conn, $_POST['name']);
    $phone  = mysqli_real_escape_string($conn, $_POST['phone']);
    $userid = $_POST['userid'];
    $update = mysqli_query($conn, "UPDATE tbl_customer SET name = '".$name."', phone = '".$phone."' WHERE id ='".$userid."'");
    if($update){
        $output  = array('status' => 'ok', 'msg' => 'Update profile berhasil...');
    } else {
        $output  = array('status' => 'error', 'msg' => 'Gagal update profile');
    }
}
if($_POST['aksi'] == 'UPDATE_PASSWORD'){
    $userid     = $_POST['userid'];
    $old_pass   = mysqli_real_escape_string($conn, $_POST['old_password']);
    $new_pass   = mysqli_real_escape_string($conn, $_POST['new_password']);
    $verif_pass = mysqli_real_escape_string($conn, $_POST['verif_password']);
    $check_pass = mysqli_query($conn, "SELECT password FROM tbl_members WHERE id = '".$userid."'");
    if(mysqli_num_rows($check_pass) < 1){
        $output  = array('status' => 'error', 'msg' => 'Password Salah!');
    } else {
        $data_pass = mysqli_fetch_array($check_pass);
        if(password_verify($old_pass, $data_pass['password'])){
            if($new_pass == $verif_pass){
                $pass_baru = password_hash($new_pass, PASSWORD_DEFAULT);
                $update    = mysqli_query($conn, "UPDATE tbl_members SET password = '".$pass_baru."' WHERE id ='".$userid."'");
                if($update){
                    $output  = array('status' => 'ok', 'msg' => 'Ganti password berhasil...');
                } else {
                    $output  = array('status' => 'error', 'msg' => 'Gagal ganti password!');
                }
            } else {
                $output  = array('status' => 'error', 'msg' => 'Konfirmasi password dengan password baru tidak sama');
            }
        } else {
            $output  = array('status' => 'error', 'msg' => 'Password Salah!');
        }
    }
}
?>
<!DOCTYPE html>
<html>
<?php include('../lib/header.phtml'); ?>
<body class="">
    <div id="wrapper">
        <?php include('../lib/sidebar.phtml'); ?>
        <?php include('../lib/headbar.phtml'); ?>
        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-sm-4">
                <h2><?=$config['name'];?></h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index-2.html"><?=$config['logo_text'];?></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Profil</strong>
                    </li>
                </ol>
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">

                <div class="col-lg-4">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Info</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="alert alert-info">Silahkan logout dan login ulang setelah mengganti password!</div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ganti Password</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <?php
                            if($output['status'] == 'ok'){
                                ?>
                                <div class="alert alert-success"><?=$output['msg'];?></div>
                                <?php
                            } elseif($output['status'] == 'error') {
                                ?>
                                <div class="alert alert-danger"><?=$output['msg'];?></div>
                                <?php
                            }
                            ?>
                            <form method="POST" action="">
                                <input type="hidden" name="aksi" value="UPDATE_PASSWORD">
                                <input type="hidden" name="userid" value="<?=$data_user['id'];?>">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Password Sekarang</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Password Sekarang" name="old_password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Password Baru</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Password Baru" name="new_password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Konfirmasi Password Baru</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Konfirmasi Password Baru" name="verif_password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" name="update_pass" type="submit">Ganti Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="footer">
            <div class="float-right">
                Made with <i class="fa fa-heart" style="color:red;"></i> by <strong>axec0de</strong>
            </div>
            <div>
                <strong>Copyright</strong> <?=$config['name'];?> - 2018
            </div>
        </div>
    </div>
    <?php include('../lib/footer.phtml'); ?>
    </body>
</html>